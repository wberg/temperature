#!/usr/bin/python

import os
import glob
import time
import re
from prometheus_client import start_http_server,Gauge
 
def read_temp_from_file():
    # Setup the dir used to read from 1-wire
    base_dir = '/sys/bus/w1/devices/'
    device_folder = glob.glob(base_dir + '28*') #[0]
    device_data = []
    for file in device_folder:
        device_file = file + '/w1_slave'
        # Read the data from the 1-wire
        f = open(device_file, 'r')
        data = f.readlines()
        f.close()
        device_data.append(data)
    return device_data

def parse_temp(data):
    result = []
    # Parse the data looking for t=XXXXX which is the temperature
    for element in data:
        for line in element:
            pattern = re.search('t=([0-9]+)', line)
            if pattern:
                # Convert XXXXX value to float and divide by 1000
                only_temp = float(pattern.group(1)) / 1000.0
                result.append(only_temp)
    return result

def main():
    # Setup a gauge to store the temp
    gTempInside = Gauge('temp_inside', 'Temperature Inside')
    hTempOutside = Gauge('temp_outside', 'Temperature Outside')
    # Start the prometheus_client web-server
    start_http_server(8080)
    # Keep on forver...
    while True:
        device_data = read_temp_from_file() 
        result = parse_temp(device_data)
        # Set the metric to last poll value
        gTempInside.set(result[0])
        hTempOutside.set(result[1])
        time.sleep(30)

if __name__ == '__main__':
    main()
