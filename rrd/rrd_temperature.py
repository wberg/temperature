#!/usr/bin/python

import os
import glob
import time
import re
import rrdtool
 
databaseFile = "temps.rrd"

rrds_to_filename = {
    "inside" : "/sys/bus/w1/devices/28-0317251327ff/w1_slave",
    "outside" : "/sys/bus/w1/devices/28-0317251327ff/w1_slave",
    }

def read_temp_from_file():
    # Setup the dir used to read from 1-wire
    base_dir = '/sys/bus/w1/devices/'
    device_folder = glob.glob(base_dir + '28*')[0]
    device_file = device_folder + '/w1_slave'
    # Read the data from the 1-wire
    f = open(device_file, 'r')
    data = f.readlines()
    f.close()
    return data

def parse_temp(data):
    # Parse the data looking for t=XXXXX which is the temperature
    for line in data:
        pattern = re.search('t=([0-9]+)', line)
        if pattern:
            # Convert XXXXX value to float and divide by 1000
            result = float(pattern.group(1)) / 1000.0
            return result

def update_rrd(result):
    # Create a template and set update to N:
    template = ""
    update = "N:"
    # For each temperature reading, append
    for rrd in rrds_to_filename:
        template += "%s:" % rrd
        update += "%f:" % result
    update = update[:-1]
    template = template[:-1]
    # Write all data to the rrd-file
    rrdtool.update(databaseFile, "--template", template, update)

def main():
    while True:
        data = read_temp_from_file() 
        result = parse_temp(data)
        update_rrd(result)
        time.sleep(30)

if __name__ == '__main__':
    main()
