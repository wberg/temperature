#!/bin/bash

/usr/bin/rrdtool create temps.rrd --start 1544978770 --step 60 \
    DS:inside:GAUGE:900:-50:50 \
    DS:outside:GAUGE:900:-50:50 \
    RRA:AVERAGE:0.5:1:525600 \
    RRA:AVERAGE:0.25:60:87600
