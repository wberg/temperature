#!/bin/sh

### Script for generating a .png file of a .rrd file, put this script into cron!

/usr/bin/rrdtool graph /var/www/html/one_day.png \
  --title "Temp (C) at home, 1d" \
  --start now-1d --end now \
  --width=640 --height=480 \
  --step=60 -v degreesC \
  DEF:temp1=/home/pi/temperature/temps.rrd:inside:AVERAGE \
  DEF:temp2=/home/pi/temperature/temps.rrd:outside:AVERAGE \
  LINE2:temp1#008000:"inside" \
  LINE2:temp2#800000:"outside" \
  HRULE:0#0000FF:"freezing"
